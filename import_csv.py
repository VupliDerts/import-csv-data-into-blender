import bpy
import pathlib
import csv

# file neighbourhood
file_loc = pathlib.Path(__file__).parent.resolve()     # this blender file
folder = pathlib.Path(file_loc).parent.resolve()       # folder file is in
parent_folder = pathlib.Path(__file__).parents[3]      # parent folder, number at end indicates how far up to go

# construct path for data paths
data = pathlib.Path(folder).joinpath('air-quality-london-time-of-day.csv')

# Blender collection for data
data_coll = bpy.data.collections['3D Data']

for ob in data_coll.objects:
    bpy.data.objects.remove(ob)

vertices = []
edges = []
faces = []

#read csv
with open(data, 'r') as csvfile:
    datareader = csv.reader(csvfile)
    next(datareader) # skip the first line
    i = 0
    for row in datareader:
        vertices.append((i,float(row[3]),0))
        i+=1
        
new_mesh = bpy.data.meshes.new('data')
new_mesh.from_pydata(vertices, edges, faces)
new_mesh.update()

new_object = bpy.data.objects.new('data_graph', new_mesh)
data_coll.objects.link(new_object)
